
int pD0 = A15;
int pA0 = A14;


void setup() {
  Serial.begin(9600);  
}

void loop() {
  // put your main code here, to run repeatedly:
  float s1 = analogRead(pD0)* 5.0 / 1023.0;
  float s2 = analogRead(pA0)* 5.0 / 1023.0;
  Serial.println("Reading voltage: ");
  Serial.print(s1);
  Serial.print(" ");
  Serial.println(s2);
  delay(300);
}
