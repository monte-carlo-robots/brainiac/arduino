// -*- lsst-c++ -*-

/*
 * This file contains the Motor class implementation to be used in the Brainiac Arduino.
 */

#ifndef BRAINIAC_ARDUINO_MOTOR_H
#define BRAINIAC_ARDUINO_MOTOR_H

#include <Encoder.h>

/**
 * Motor class to interface with the motors at the Brainiac.
 *
 * In this class you can read the encoder pulses, set the velocity (voltage) to the motors and reverse the rotation.
 *
 */
class Motor{
    bool reverse_ = false;
    uint8_t velocity_ = 0;
    int rot_port1;
    int rot_port2;
    int vel_port;
    Encoder enc;

public:
    /**
     * Instantiates a motor object, just fills the attributes. To setup the ports call `Motor::setup()` method.
     *
     * Note that the encoders pins should be set as increases the counter in forward move. They are passed in order to
     * the Encoder constructor from Encoder library.
     *
     * @param motor_port1: One of the pins to control the rotation direction. Should be `HIGH` to move forward.
     * @param motor_port2: One of the pins to control the rotation direction. Should be `LOW` to move forward.
     * @param motor_vel: The pin to control the motor velocity. Send a PWM in range 0-5V going from 0 to maximum speed.
     * @param enc_port1: One of the pins with the encoder phase. Might be capable of `AttachInterrupt`.
     * @param enc_port2: One of the pins with the encoder phase. May be capable of `AttachInterrupt`.
     */
    Motor(int motor_port1, int motor_port2, int motor_vel, int enc_port1, int enc_port2);

    /// Set the motor pins as OUTPUT, writes 0 to velocity pin and set others in forward configuration.
    void setup();

    /// Returns a `Boolean` indicating if the motor is in reverse rotation or not.
    bool reverse();

    /**
     * Set the reverse rotation.
     *
     * @param value: Boolean indicating if should do reverse rotation or not.
     */
    void reverse(bool value);

    /// Get the actual motor velocity command.
    uint8_t velocity();

    /**
     * Set the velocity.
     *
     * @param velocity: The value to be writen with `analogWrite` to the motor.
     */
    void velocity(uint8_t value);

    /// A shortcut to set the velocity to 0, equals to `Motor::velocity(0)`
    void disable();

    /// Returns the encoder actual readings state, calls `Encoder::read()`.
    int32_t read();
};


#endif //BRAINIAC_ARDUINO_MOTOR_H
