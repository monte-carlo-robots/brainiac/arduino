#include <ros.h>
#include <sensor_msgs/Illuminance.h>
#include <brainiac_control/MotorCmd.h>
#include <brainiac_control/MotorEnc.h>
#include "motor.h"

// Pins to read the IR sensor
#define IR_RIGHT A0
#define IR_LEFT A1

// Pins to read the encoder
#define ENC2_L 21
#define ENC1_L 20
#define ENC2_R 19
#define ENC1_R 18

// Pins attached to the H bridge driver input
#define VEL_R 5
#define PIN2_R 6
#define PIN1_R 7
#define PIN2_L 8
#define PIN1_L 9
#define VEL_L 10

int seq=0;

Motor motor_left(PIN1_L, PIN2_L, VEL_L, ENC1_L, ENC2_L);
Motor motor_right(PIN1_R, PIN2_R, VEL_R, ENC1_R, ENC2_R);

ros::NodeHandle nh;

brainiac_control::MotorEnc enc_msg;
ros::Publisher motor_pub("wheels", &enc_msg);

sensor_msgs::Illuminance ir_msg;
ros::Publisher ir_sens_pub("ir_sensor", &ir_msg);

/// ROS Callback to receive the velocity command and send it to the motors.
void motors_cb(const brainiac_control::MotorCmd& cmd);
ros::Subscriber<brainiac_control::MotorCmd> motor_sub("motors", &motors_cb);

void setup() {
  motor_right.setup();
  motor_left.setup();
  nh.getHardware()->setBaud(115200);
  nh.initNode();
  nh.advertise(motor_pub);
  nh.subscribe(motor_sub);
  nh.advertise(ir_sens_pub);
}

void loop() {
  enc_msg.left = motor_left.read();
  enc_msg.right = motor_right.read();
  enc_msg.header.stamp = nh.now();
  motor_pub.publish(&enc_msg);
  update_ir_msg();
  ir_sens_pub.publish(&ir_msg);
  nh.spinOnce();
  ++seq;
  delay(10);
}

void motors_cb(const brainiac_control::MotorCmd& cmd) {
  motor_left.reverse(cmd.inverse_left);
  motor_left.velocity(cmd.left);
  motor_right.velocity(cmd.right);
  motor_right.reverse(cmd.inverse_right);
}

float update_ir_msg(){
  ir_msg.variance = 0.001;
  ir_msg.illuminance = (analogRead(IR_RIGHT)) * 255.0 / (1024.0 );
  ir_msg.header.stamp = nh.now();
  ir_msg.header.frame_id = "";
  ir_msg.header.seq = seq;
}
