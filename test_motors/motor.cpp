// -*- lsst-c++ -*-

/*
 * This file contains the Motor class implementation to be used in the Brainiac Arduino.
 */

#include "motor.h"

Motor::Motor(int motor_port1, int motor_port2, int motor_vel, int enc_port1, int enc_port2)
        : rot_port1(motor_port1), rot_port2(motor_port2), vel_port(motor_vel), enc(enc_port1, enc_port2) {

}

void Motor::setup() {
    pinMode(rot_port1, OUTPUT);
    pinMode(rot_port2, OUTPUT);
    pinMode(vel_port, OUTPUT);

    reverse(false);
    disable();
}

bool Motor::reverse() {
    return reverse_;
}

void Motor::reverse(bool value) {
    reverse_ = value;
    digitalWrite(rot_port1, reverse_);
    digitalWrite(rot_port2, !reverse_);
}

uint8_t Motor::velocity() {
    return velocity_;
}

void Motor::velocity(uint8_t value) {
    velocity_ = value;
    analogWrite(vel_port, velocity_);
}

void Motor::disable() {
    velocity(0);
}

int32_t Motor::read() {
    return enc.read();
}
