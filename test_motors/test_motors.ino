#include "motor.h"

// Pins to read the IR sensor
#define IR_RIGHT A0
#define IR_LEFT A1

// Pins to read the encoder
#define ENC2_L 21
#define ENC1_L 20
#define ENC2_R 19
#define ENC1_R 18

// Pins attached to the H bridge driver input
#define VEL_R 5
#define PIN1_R 6
#define PIN2_R 7
#define PIN1_L 8
#define PIN2_L 9
#define VEL_L 10

int var1 = 0;
char reading;
bool rev = false;

Motor motor_left(PIN1_L, PIN2_L, VEL_L, ENC1_L, ENC2_L);
Motor motor_right(PIN1_R, PIN2_R, VEL_R, ENC1_R, ENC2_R);

void setup() {
  motor_right.setup();
  motor_left.setup();
  Serial.begin(115200);
}

void loop() {
  if(Serial.available()){
    reading = Serial.read();
    switch (reading){
      case 'w':
        ++var1;
        break;
      case 's':
        --var1;
        break;
      case 'p':
        var1 = 255;
        break;
      case 'o':
        var1 = 0;
        break;
      case 'r':
        rev = !rev;
        motor_right.reverse(rev);
        break;
    }
    motor_left.velocity(0);
    motor_right.velocity(var1);
  }
  Serial.println(var1);
  
  delay(10);
}
